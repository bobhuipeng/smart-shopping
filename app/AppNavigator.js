import React from 'react';
import { View, Text } from 'react-native'
import { createDrawerNavigator } from 'react-navigation';

import Ionicons from 'react-native-vector-icons/Ionicons';

import HistoryScreen from './screens/HistoryScreen';
import ShoppingListScreen from './screens/ShoppingListScreen';
import SystemSettingScreen from './screens/SystemSettingScreen';

const ShoppingHistory = {
  screen: HistoryScreen,
  navigationOptions: {
    header: 'Shopping History',
    drawerLabel: 'History',
    drawerIcon: (focused,tintColor) =>(
      <View style={{backgroundColor: tintColor}}>
      <Text>
        <Ionicons name={focused ? 'ios-clipboard' : 'ios-clipboard-outline' } size={26}/>
      </Text>
      </View>

    ),
  }
}


const ShoppingList = {
  screen: ShoppingListScreen,
  navigationOptions: {
    drawerLabel: 'Shopping List',
    drawerIcon: (focused,tintColor) =>(
      <View style={{backgroundColor: tintColor}}>
      <Text>
        <Ionicons name={focused ? 'ios-cart-outline' : 'ios-cart-outline' } size={26}/>
      </Text>
      </View>
      
    ),
  }
}

const SystemSetting = {
  screen: SystemSettingScreen,
  navigationOptions: {
    drawerLabel: 'System Setting',
    drawerIcon: (focused,tintColor) =>(
      <View style={{backgroundColor: tintColor}}>
        <Text>
          <Ionicons name={focused ? 'ios-cog' : 'ios-cog-outline' } size={26}/>
        </Text>
      </View>
    ),
  }
}

const RouteConfig = {
  initialRouteName :'ShoppingList',
  headerMode: 'none',
  mode: 'modal',
  contentOptions:{
    activeTintColor: '#e91e63',
    itemsContainerStyle: {
      marginVertical: 0,
    },
    iconContainerStyle: {
      opacity: 1
    }
  },
  navigationOptions: {
    gesturesEnabled: false,
  },
  transitionConfig: () => ({
    transitionSpec: {
      duration: 300,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
    },
    screenInterpolator: sceneProps => {
      const { layout, position, scene } = sceneProps;
      const { index } = scene;

      const height = layout.initHeight;
      const translateY = position.interpolate({
        inputRange: [index - 1, index, index + 1],
        outputRange: [height, 0, 0],
      });

      const opacity = position.interpolate({
        inputRange: [index - 1, index - 0.99, index],
        outputRange: [0, 1, 1],
      });

      return { opacity, transform: [{ translateY }] };
    },
  }),
}



const AppNavigator = createDrawerNavigator({
  ShoppingList: ShoppingList,
  History: ShoppingHistory,
  Setting: SystemSetting
},RouteConfig)

export default AppNavigator;