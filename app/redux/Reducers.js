import CTS from './Constants'
import {
  combineReducers
} from 'redux'
import {
  increaseItemCount
} from './Actions';
// import { updateItem } from './Actions';
// import { addItem, getCurrentSPList } from './Actions';

// import {defaultState} from './index'
import currentSPList from './shoppingListReducer'
import historicalSPList from './HistorycalReducer'
import sysSetting from './SysSettingReducer'

var defaultState = {
  currentSPList: {
    name: '',
    items: [{
        id: 1,
        name: 'apple',
        quantity: 2,
        isDone: false
      },
      {
        id: 2,
        name: 'grape',
        quantity: 100,
        isDone: false
      },
      {
        id: 3,
        name: 'butter',
        quantity: 30,
        isDone: false
      },

    ]
  },
  historicalSPList: {},
  habitItems: null,
  sysSetting: {
    isSimpleMode: false
  }
  // LoginUser:{
  //   username:null,
  //   token:null
  // }
  }

//TODO: understand different reducer did not share state



/**
 * shopping list operation
 * **/
shoppingListOp = (state = {}, action) => {
  switch (action.type) {
    case CTS.GET_CURRENT_SHOPPING_LIST:
      return getCurrentSPList(state);
    default:
      return state;
  }
}

//TODO: not finished
getCurrentSPList = (state, action) => {
  return state
}


//User operation

userOp = (state = {}, action) => {
  switch (action.type) {
    case CTS.GET_LOGIN_USER:
      return state;
    default:
      return state;
  }
}

/**
 * user habit items
 * */
habitItems = (state = [], action) => {
  switch (action.type) {
    case CTS.GET_USER_HOBIT_ITEM_LIST:
      return state; //TODO: not finished
    default:
      return state;
  }
}


/**
 * system setting
 * */
systemOp = (state = {}, action) => {
  switch (action.type) {
    case CTS.GET_SYS_SETTING:
      return state;
    case CTS.UPDATE_SYS_SETTING:
      return state;

    default:
      return state;
  }
}


export default combineReducers({
  currentSPList,
  habitItems,
  historicalSPList,
  sysSetting
  // shoppingListOp,
  // historyOp,
  // userOp,
  // historyOp,
  // systemOp
})