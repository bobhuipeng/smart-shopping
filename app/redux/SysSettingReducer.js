import CST from '../redux/Constants'

// const systemConfig = {
//   MODEL_NORMAL:'NORMAL',
//   MODEL_SIMPLE:'SIMPLE',
// }


let sysSetting = {
    isSimpleMode: false
  }



sysSettingReducer = (state = sysSetting,action) => {

  switch(action.type){
    case CST.TONGLE_MODEL_IS_SIMPLE: console.log(state); return {...state,isSimpleMode:!state.isSimpleMode};
    case CST.RESET_SYS_DEFAULT: console.log(state); return {isSimpleMode: false};
    default: return state;
  }
  
}

export default sysSettingReducer