import CTS from './Constants'
import {
  combineReducers
} from 'redux'
import {
  increaseItemCount
} from './Actions';
// import { updateItem } from './Actions';
// import { addItem, getCurrentSPList } from './Actions';

// import {defaultState} from './index'


var defaultState = 
 [{
        id: 1,
        name: 'apple',
        quantity: 2,
        isDone: false
      },
      {
        id: 2,
        name: 'grape',
        quantity: 100,
        isDone: false
      },
      {
        id: 3,
        name: 'butter',
        quantity: 30,
        isDone: false
      },

    ]

//TODO: understand different reducer did not share state


//item operation in shopping list
currentSPList = (state = defaultState, action) => {
  switch (action.type) {
    case CTS.ADD_ITEM:
      return addItemToShoppingList(state, action.payload);
    case CTS.UPDATE_ITEM:
      return updateItemInShoppingList(state, action.payload);
    case CTS.DELETE_ITEM:
      return deleteItemFromSPList(state, action.payload);

    case CTS.UPDATE_ITEM_QUANTITY:
      return updateItemQuantity(state, action.payload);
    case CTS.INCREASE_ITEM_COUNT:
      return increaseItemQuantity(state, action.payload);
    case CTS.DECREASE_ITEM_COUNT:
      return decreaseItemQuantity(state, action.payload);
    case CTS.DONE_ITEM:
      return doneItem(state, action.payload);
    case CTS.REDO_ITEM:
      return redoItem(state, action.payload);
    default:
      return state;
  }
}

updateItemQuantity = (state, item) => state.map(it =>
      (it.name === item.name)
        ? {...it, quantity: item.quantity}
        : it
    )

changeOneItemQuantity = (state, item, operat = 1, changeSize = 1) => {

  if (isNaN(item.quantity)) {
    return state
  }
  let newItem = { ...item}
  newItem.quantity = newItem.quantity + changeSize * operat

  return updateItemQuantity(state, newItem)
}

increaseItemQuantity = (state, action) => {
  return changeOneItemQuantity(state, action)
}

decreaseItemQuantity = (state, action) => {
  return changeOneItemQuantity(state, action, -1)
}


doneItem = (state , item) => {
  let newitem = { ...item
  }
  newitem.isDone = true
  return updateItemInShoppingList(state, newitem)
}


redoItem = (state, item) => {
  let newitem = { ...item
  }
  newitem.isDone = false
  return updateItemInShoppingList(state, newitem)
}


updateItemInShoppingList = (state, item) => 
 state.map(it =>
      (it.name === item.name)
        ? item
        : it
    )

addItemToShoppingList = (state, item) => {
  return [...state,item]
}


deleteItemFromSPList = (state, item) => {
  return state.filter(it => it.name !== item.name)
}


export default currentSPList
