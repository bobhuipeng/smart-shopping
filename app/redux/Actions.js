import CTS from './Constants'

import {shoppingListAPI} from '../api'

actionGenerator = (actionType) => {
  return (payload)=>({
    type: actionType,
    error:null,
    payload:payload,
  })
}

actionGeneratorNoPram = (actionType) => {
  return ()=>({
    type: actionType,
    error:null,
    payload:null,
  })
}


export const addItem = actionGenerator(CTS.ADD_ITEM)
export const updateItem = actionGenerator(CTS.UPDATE_ITEM)
export const deleteItem = actionGenerator(CTS.DELETE_ITEM)


export const updateItemQuantity = actionGenerator(CTS.UPDATE_ITEM_QUANTITY)


export const increaseItemCount = actionGenerator(CTS.INCREASE_ITEM_COUNT)
export const decreaseItemCount = actionGenerator(CTS.DECREASE_ITEM_COUNT)
export const doneItem = actionGenerator(CTS.DONE_ITEM)
export const redoItem = actionGenerator(CTS.REDO_ITEM)


export const getCurrentSPList = actionGenerator(CTS.GET_CURRENT_SHOPPING_LIST)

export const getLoginUser = actionGenerator(CTS.GET_LOGIN_USER)
export const getUserHobitItemList = actionGenerator(CTS.GET_USER_HOBIT_ITEM_LIST)

export const fetchShoppingHistory = actionGenerator(CTS.FETCH_SHOPPING_HISTORY) 

/**
 * System setting
 */
export const getSysSetting = actionGenerator(CTS.GET_SYS_SETTING)
export const updateSysSetting = actionGenerator(CTS.UPDATE_SYS_SETTING)

export const tongleModelIsSimple = actionGeneratorNoPram(CTS.TONGLE_MODEL_IS_SIMPLE)
export const resetSysDefault = actionGeneratorNoPram(CTS.RESET_SYS_DEFAULT)


export const  getShoppingListHistory = username => (dispatch, getState) => {
  let isFetching = getState().historicalSPList.isFetching
  console.log('username',username,isFetching)
  if(! getState().historicalSPList.isFetching){
     //TODO:wait
    dispatch(fetchShoppingHistory(true))
     fetch('http://192.168.1.106:9090/shopping/list/stormswan')
      .then( response =>response.json())
      .then( shoppingListHistory =>{
        dispatch(actionGenerator(CTS.GET_SHOPPNG_LIST_HIST)(shoppingListHistory))
        dispatch(fetchShoppingHistory(false)) 
      }
      ).catch(error =>{
        console.log('Error in get shopping history: ',error)
        dispatch(fetchShoppingHistory(false))
      })
  }
}






 