import CTS from './Constants'

historicalSPList = {
  isFetching: false,
  historyList: []
}


/**
 * history operation
 * */
historicalSPList = (state = historicalSPList , action) => {
  switch (action.type) {
    case CTS.FETCH_SHOPPING_HISTORY:
      return _updateFetchingStatus(state,action.payload); 
    case CTS.GET_SHOPPNG_LIST_HIST:
      return _getShoppingListHistory(state,action.payload); 
    default:
      return state;
  }
}

_getShoppingListHistory = (state, shoppingList) =>{
  return {...state, historyList: shoppingList.shoppingHistory}
}

_updateFetchingStatus = (state,fetchStatus) =>{
  return {...state, isFetching: fetchStatus}
}



export default historicalSPList