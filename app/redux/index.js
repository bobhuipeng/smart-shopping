import thunk from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'
// import { composeWithDevTools } from 'redux-devtools-extension';
import appReducer from './Reducers'
import {MODEL_SIMPLE,MODEL_NORMAL} from './Constants'

// import Actions from './Actions'
// import Reducers from './Reducers'


var defaultState = {
  currentSPList:
[{
    id: 1,
    name: 'apple',
    quantity: 2,
    isDone:false
},
{
    id: 2,
    name: 'grape',
    quantity: 100,
    isDone:false
},
{
    id: 3,
    name: 'butter',
    quantity: 30,
    isDone:false
},

],
  historicalSPList:{
    isFetching: false,
    historyList: []
  },
  habitItems: null,
  // sysSetting:{
  //   model:MODEL_NORMAL,

  // },
  // LoginUser:{
  //   username:null,
  //   token:null
  // }
}

var shoppingList = {
  name: '',
  items: [
    item
  ]
}

var item ={
  name: '',
  quantity_type:'',
  quantity:1,
}

export default (initialState=defaultState) => {
	console.log(initialState);
  // var store = createStore(appReducer,initialState,composeWithDevTools(applyMiddleware(thunk)))
  var store = createStore(appReducer,initialState,applyMiddleware(thunk))
	console.log('current system state',store.getState());
	
	return store
}

export {

}