import React, { PureComponent } from 'react'
import { Text, View, TouchableOpacity, Icon } from 'react-native'

import PropTypes from 'prop-types'

/**
 * this component is dealing with keep holding the button. When hold
 * the button, it will process the function 'whenHold'. If the holding
 * more than 5 seconds, then the interval speed set to 1/5 of interval
 * time
 */
export default class HoldingButton extends PureComponent {

  constructor(props) {
    super(props);
    this.timer = null;
    this.startHold = null;
    this.defaultInterval = 200
  }

  static propTypes = {
    whenHold: PropTypes.func.isRequired,
    interval: PropTypes.number,
  }

  componentWillMount(){
    if(this.props.interval){
      this.defaultInterval = this.props.interval
    }
  }

  holdingPress = () => {
    if (this.startHold === null) {
      this.startHold = new Date();
    }

    this.props.whenHold();//this.startHold, ...this.props
    this.timer = setTimeout(this.holdingPress, this.setInterval());

  }
  
  setInterval = () =>{
    if((new Date() - this.startHold) >  5000){
      return this.defaultInterval/5
    }else{
      return this.defaultInterval
    }
  }

  stopTimer = () => {
    clearTimeout(this.timer);
    this.startHold = null;
  }

  render() {
    return (
        <TouchableOpacity onPressIn={this.holdingPress} onPressOut={this.stopTimer} style={this.props.style}>
            {this.props.children}
        </TouchableOpacity>
    )
  }
}