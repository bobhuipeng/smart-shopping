import React, { Component } from 'react'
import { TouchableOpacity , View } from 'react-native'
import PropTypes from 'prop-types'

export class DoubleTap extends Component {
  constructor(props) {
    super(props)

    this.state = {
      lastTap: 0,
      doubleTapCount:0,
    }
  }


  tapOn = () => {
    const tapInterval = new Date().getTime() - this.state.lastTap
    
    if(tapInterval < this.props.interval){
        this.setState({
            doubleTapCount: this.state.doubleTapCount+1,
        })
        this.props.doubleTap( this.state.doubleTapCount,this.state.lastTap,this.getRestPorps())
    }

    this.setState({
        lastTap: new Date().getTime(),
    })

  }
  
  getRestPorps =() =>{
    
    const other = {...this.props}

    delete other.component
    delete other.interval
    delete other.doubleTap
    return other
  }

  render() {
    const Component = this.props.component
    restProps = this.getRestPorps()
    return (
        <Component  {...restProps} onPress={() => this.tapOn()}>  
          {this.props.children}
        </Component  >
    )
  }
}


DoubleTap.defaultProps = {
    interval: 350,
    doubleTap: () => console.log('Button is tapped twice'),
    component: TouchableOpacity
}

DoubleTap.propTypes = {
    doubleTap: PropTypes.func,
    interval: PropTypes.number,
    component: PropTypes.func
}

export default DoubleTap