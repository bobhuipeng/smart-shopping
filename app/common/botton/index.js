import HoldingButton from './HoldingButton'
import DoubleTap from './DoubleTap'

export {
    HoldingButton,
    DoubleTap
} 