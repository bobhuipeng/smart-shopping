
const baseUrl = 'http://192.168.1.106:9090/';
const shopingList = 'shopping/list/';

//TODO: udpate real login username with  username =tempLoginUsername
const tempLoginUsername = 'stormswan'

export default {
    async  getShoppingHistory(username) {
      username =tempLoginUsername
        try {
          let url = baseUrl+shopingList+username
          let response = await fetch(url);
          let responseJson = await response.json();
          return responseJson.shoppingHistory;
        } catch (error) {
          console.error(error);
        }
    },
    async  submitShoppingList(username,shoppingList) {
      username =tempLoginUsername
      try {
        let url = baseUrl+shopingList+username
        
        //it will insert or update the shopping
        //base on the list name
        let response = await fetch(url, {
          method: 'PUT',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body:  JSON.stringify(shoppingList),
        });


        let responseJson = await response.json();        
        return responseJson;
      } catch (error) {
        console.error(error);
      }
  },
  async  getShoppingItemHabit(username) {
    username =tempLoginUsername
    try {
      let url = baseUrl+'shopping/habit/'+username      
      let response = await fetch(url, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({username:username}),
      });

      let responseJson = await response.json();

      
      return responseJson.shoppingHistory;
    } catch (error) {
      console.error(error);
    }
  }
}

