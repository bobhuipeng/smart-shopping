import React, {
    Component
} from 'react'
import PropTypes from 'prop-types'
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableHighlight,
    TouchableOpacity,
    Alert,

} from 'react-native'

import ModalDropdown from 'react-native-modal-dropdown';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Autocomplete from 'react-native-autocomplete-input';


export default class AddItem extends Component {
    constructor(props){
        super(props)
        this.state = {
          quantity: 1,
            query:'',
            name:'',
            unit:'',
            historyItems:[],
            currentShoppingItems:[],
        }
    }

    static propTypes = {
        addNewItem: PropTypes.func.isRequired,
        updateItemQuantity: PropTypes.func.isRequired,
        historyItems: PropTypes.array.isRequired,
        itemList: PropTypes.array.isRequired,
    }

    submit = () => {

      //require the item name
        if(this.state.name.trim() === ''){
            alert('Please input item name!')
            return;
        }

        let newItem ={name:this.state.name.trim(), quantity:parseInt(this.state.quantity), unit: this.state.unit, isDone: false}

        existItem =this.props.itemList.filter(item => item.name === newItem.name);
        //if item has added, ask update quantity or not
        if(existItem !== undefined && existItem.length > 0){
            
            return Alert.alert(
                'Duplicate item!',
                `The Item has been added, do you want update the count from to `,
                [
                    {text: 'Update', onPress: () =>{this.props.updateItemQuantity(newItem)}},
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed!')},
                ]
            )
        }else{
          this.props.addNewItem(newItem)
        }
    }
    
    findItem = (query) =>{
        if(query === ''){
             return [];
        }
        return this.props.historyItems.filter(item =>item.name.toLowerCase().includes(query.toLowerCase()))
    }

  //TODO: the keyboardType='numeric' is not working well on IPad, it only show the number input keyboard, but still can change back to normal
  render() {
    const { query } = this.state;
    const data = this.findItem(query)
    const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
    return (
    <View style={styles.addNewItemGroup}>
        <Autocomplete
            autoCapitalize="none"
            autoCorrect={true}
            data={data.length === 1 && comp(query, data[0].name) ? [] : data}
            defaultValue={query}
            onChangeText={text => this.setState({ query: text, name: text })}
            placeholder="Enter item name"
            containerStyle={styles.autocompleteContainer}
            renderItem={({ name, quantity }) => (
                <TouchableOpacity onPress={() => this.setState({ query: name, name: name, quantity: quantity })}>
                <Text style={styles.autoCompleteItemText}>
                    {name} - ({quantity})
                </Text>
                </TouchableOpacity>
            )}
        />
        <TextInput 
            value={this.state.quantity.toString()} 
            onChangeText = {(txt) => this.setState({quantity:txt})} 
            style={[styles.buttonBase,styles.itemCoutn]}

            keyboardType='numeric'
            maxLength={10} 
        /> 
        <ModalDropdown 
            options={unitDrops} 
            defaultValue='Type'
            onSelect = {(idx, value) => this.setState({unit: value})}
            style={[styles.buttonBase,styles.typeInput]}
            textStyle={{fontSize:16}}
            dropdownTextStyle={styles.dropdownTextStyle} 
            />
        <Text onPress={this.submit} >        
        <Ionicons
            name={true ? 'ios-basket' : 'ios-basket-outline'}
            size={imageSize}
        />
        </Text>
    </View>
      
    )
  }
}

const unitDrops = [' ','Kg', 'g', 'bag', 'bottle', 'box', '$',]

const imageSize =30
const styles = StyleSheet.create({
    addNewItemGroup: {
      flexDirection: 'row',
      margin: 5,
      padding: 2,
      zIndex: 1,
    },
    autocompleteContainer: {
      flex: 0.9,
      marginLeft: 10,
      marginRight: 10,
    //   backgroundColor: 'blue'
    },
    autoCompleteItemText: {
      fontSize: 19,
      borderWidth: 2,
      borderColor: 'white',
      borderRadius: 9,
      backgroundColor: '#fafce6'
    },
    typeInput:{
        borderRadius:9,
        paddingLeft: 5,
    },
    buttonBase:{
        width: 70,
        height: 30,
        margin: 2,
        backgroundColor: '#bfd7ec',
    },
    itemCoutn:{
        fontSize: 16,
        width: 70,
        borderRadius:9,
        paddingLeft: 5,
    },
    dropdownTextStyle:{
        fontSize: 16,
    },
    descriptionContainer: {
      // `backgroundColor` needs to be set otherwise the
      // autocomplete input will disappear on text input.
      backgroundColor: '#F5FCFF',
      marginTop: 8
    },
    infoText: {
      textAlign: 'center'
    },
    titleText: {
      fontSize: 18,
      fontWeight: '500',
      marginBottom: 10,
      marginTop: 10,
      textAlign: 'center'
    },
    directorText: {
      color: 'grey',
      fontSize: 12,
      marginBottom: 10,
      textAlign: 'center'
    },
    openingText: {
      textAlign: 'center'
    }
  });