import React, {
    Component
} from 'react'
import PropTypes from 'prop-types'
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from 'react-native'

import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Swipeable from 'react-native-swipeable';

import {HoldingButton, DoubleTap} from '../../../common/'
// import { takeWhile } from 'rxjs/operators';

export default class ShoppingItem extends Component {
    static propTypes = {
        item: PropTypes.object.isRequired,
        shoppingFinished: PropTypes.bool.isRequired,
        increaseItemCount: PropTypes.func.isRequired,
        decreaseItemCount: PropTypes.func.isRequired,
        finishItem: PropTypes.func.isRequired,
        deleteItem: PropTypes.func.isRequired,
        redoItem:   PropTypes.func.isRequired
    }

    constructor(props) {
        super(props)
        this.state ={
            leftActionActivated: false
        }
    }

    //TODO: how to set the focused of Ionicons <Ionicons name={focused ? 'ios-arrow-dropleft' : 'ios-arrow-dropleft-outline'}

    increaseCount = () => {
        this.props.increaseItemCount(this.props.item)
    }

    decreaseCount = () => {
        this.props.decreaseItemCount(this.props.item)
    }

    finishItem = () => {
      this.props.finishItem(this.props.item)
    }

    redoItem = () => {
        this.props.redoItem(this.props.item)
      }
    
    removeItem = () => {
        this.props.deleteItem(this.props.item)
    }

    doubleTapOnItem =(doubleTapCount,lastTap,{item})=>{
        if(item.isDone ) {
            this.redoItem()
        }else{
            this.finishItem()  
        }
        //TODO: the compoent will rebuilt when do finishItem(),
        //but not rebuilt when do  redoItem()
        // console.log(doubleTapCount);
        
        // if(doubleTapCount % 2 == 0 ) {
        //     this.finishItem() 
        // }else{ 
        //     this.redoItem()  
        // }
    }

    leftContent = <Text>Pull to activate</Text>;
    rightButtons = [
        <Text  onPress ={this.finishItem}>
        <Ionicons
        name={true ? 'ios-checkmark-circle': 'ios-checkmark-circle-outline'}
        size={imageSize}
        />
    </Text>,
    <Text  onPress ={this.removeItem}>
        <Ionicons
        name={true ? 'ios-trash' : 'ios-trash-outline'}
        size={imageSize}
        />
    </Text>
      ];

    redoShoppingBotton = <View style={styles.leftActionButtonGroup}>
                            <Text onPress ={this.redoItem}>          
                                <MaterialIcons name={'redo'} size={imageSize} />
                            </Text>
                        </View>  

  render() {
    let detailsBackgroudColor =  this.props.item.isDone? 'lawngreen' : (this.props.shoppingFinished ? 'lightgray'  : 'powderblue')
    let shoppingItemDetails =  
        <View style={[styles.shoppingItemDetails,{backgroundColor: detailsBackgroudColor},(this.props.simpleMode? styles.simpleMode:styles.fullMode )]}>
        <DoubleTap doubleTap={this.doubleTapOnItem} item={this.props.item}>
        <Text style={styles.itemDetailText}>{this.props.item.name} : {this.props.item.quantity} - {this.props.item.unit}</Text>
        </DoubleTap>       
        </View>       
      if(this.props.item.isDone || this.props.shoppingFinished){
         return(
        <View style={styles.shoppingItem}>
            {shoppingItemDetails}
            {!this.props.shoppingFinished && this.redoShoppingBotton}
        </View>
        )
      }else{
        return (
          <Swipeable  rightButtons={ this.rightButtons}>
            <View style={styles.shoppingItem}>
                {shoppingItemDetails}
                {
                    !this.props.simpleMode && 
                    <View style={styles.leftActionButtonGroup}>
                    <HoldingButton whenHold ={this.decreaseCount}>          
                        <MaterialIcons
                        name={'remove'}
                        size={imageSize}
                        />
                    </HoldingButton>
                    <HoldingButton  whenHold ={this.increaseCount}>
                        <MaterialIcons
                        name={'add'}
                        size={imageSize}
                        />
                    </HoldingButton>
                </View>
                }
          </View>
          </Swipeable>)
      }
  }
}

const imageSize = 30

const styles = StyleSheet.create({
    shoppingItem: {
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row',
        margin: 2,
        padding: 2
    },
    shoppingItemDetails: {
        height: 40,
        borderRadius: 12,
        borderWidth: 0.5,
        borderColor: '#d6d7da',
    },
    fullMode:{
        flex: 0.75,
    },
    simpleMode:{
        flex: 0.9,
    },
    itemDetailText:{
        fontSize: 19,
        fontFamily: 'Verdana',
        paddingLeft:9
    },
    leftActionButtonGroup: {
        flex: 0.25,
        justifyContent: 'space-around',
        flexDirection: 'row',
        backgroundColor: 'white',
        borderRadius: 12,
        borderWidth: 2.5,
        borderColor: 'darkorange',
        margin:0,
        padding:0,
    },
    addQuantityButton:{
        // flex: 0.5,
    },
    minorQuantityButton:{
        // flex: 0.5,
    },
    container: {
        flex: 1,
        paddingTop: 20
    },
    listItem: {
        height: 75,
        alignItems: 'center',
        justifyContent: 'center'
    },
    leftSwipeItem: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingRight: 20
    },
    rightSwipeItem: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft: 20
    },

});