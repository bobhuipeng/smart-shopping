import React from 'react'
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableHighlight,
  TouchableOpacity,
  Alert,

} from 'react-native'
import PropTypes from 'prop-types'
import ShoppingItem from '../containers/ShoppingItem'

const ListShoppingItems = ({ shopingItems, shoppingFinished,simpleMode }) => (
  <View>
    {shopingItems.map(item =>
      <ShoppingItem key={item.name} item={ item} shoppingFinished ={shoppingFinished}  simpleMode={simpleMode}/>

    )}
  </View>
)

// TodoList.propTypes = {
//   todos: PropTypes.arrayOf(PropTypes.shape({
//     id: PropTypes.number.isRequired,
//     completed: PropTypes.bool.isRequired,
//     text: PropTypes.string.isRequired
//   }).isRequired).isRequired,
//   toggleTodo: PropTypes.func.isRequired
// }

export default ListShoppingItems
