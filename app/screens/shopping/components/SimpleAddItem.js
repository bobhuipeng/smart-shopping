import React, { Component } from 'react'
import { Text, View,TouchableOpacity, TextInput, StyleSheet,} from 'react-native'

import Autocomplete from 'react-native-autocomplete-input';
import Ionicons from 'react-native-vector-icons/Ionicons';

export class SimpleAddItem extends Component {
  constructor(props){
    super(props)
    this.state = {
      quantity: 1,
      query:'',
      historyItems:[]
    }
  }

  componentDidMount(){
    this.myTextInput.focus()
  }

  submit = (event) => {
    // event.preventDefault();
    if(this.state.name === '' || this.state.name === undefined){
        alert('Please input item name!')
        return;
    }
   
    // count:parseInt(this.state.count), unit: this.state.unit

    this.props.addNewItem({name:this.state.name,})
    this.setState({name :'',query:''})

    //TODO: let the focus back when submit. The following two are not
    //working
    // event.nativeEvent.focus()
    //this.myTextInput.focus()
  }

  findItem = (query) =>{
    if(query === ''){
         return [];
    }
    return this.props.historyItems.filter(item =>item.name.toLowerCase().includes(query.toLowerCase()))
  }

  render() {
    const { query } = this.state;
    const data = this.findItem(query)
    const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();

    return (
      <View  style={styles.addNewItemGroup}>
        <Autocomplete
            ref={(ref) => this.myTextInput = ref} 
            autoCapitalize="none"
            autoCorrect={true}
            data={data.length === 1 && comp(query, data[0].name) ? [] : data}
            defaultValue={query}
            onChangeText={text => this.setState({ query: text, name: text })}
            placeholder="Enter item name"
            onSubmitEditing={this.submit}
            containerStyle={styles.autocompleteContainer}
            renderItem={({ name, quantity }) => (
                <TouchableOpacity onPress={() => this.setState({ query: name, name: name, quantity: quantity })}>
                <Text style={styles.autoCompleteItemText}>
                    {name} - ({quantity})
                </Text>
                </TouchableOpacity>
            )}
        />
        <Text onPress={this.submit} >        
         <Ionicons
            name={'ios-basket'}
            size={imageSize}
          />
        </Text>
      </View>
    )
  }
}


const imageSize =30
const styles = StyleSheet.create({
    addNewItemGroup: {
      flexDirection: 'row',
      margin: 5,
      padding: 2,
      zIndex: 1,
    },
    autocompleteContainer: {
      flex: 0.9,
      marginLeft: 10,
      marginRight: 10,
    //   backgroundColor: 'blue'
    },
    autoCompleteItemText: {
      fontSize: 19,
      borderWidth: 2,
      borderColor: 'white',
      borderRadius: 9,
      backgroundColor: '#fafce6'
    },
    typeInput:{
        borderRadius:9,
        paddingLeft: 5,
    },
    buttonBase:{
        width: 70,
        height: 30,
        margin: 2,
        backgroundColor: '#bfd7ec',
    },
    itemCoutn:{
        fontSize: 16,
        width: 70,
        borderRadius:9,
        paddingLeft: 5,
    },
    dropdownTextStyle:{
        fontSize: 16,
    },
    descriptionContainer: {
      // `backgroundColor` needs to be set otherwise the
      // autocomplete input will disappear on text input.
      backgroundColor: '#F5FCFF',
      marginTop: 8
    },
    infoText: {
      textAlign: 'center'
    },
    titleText: {
      fontSize: 18,
      fontWeight: '500',
      marginBottom: 10,
      marginTop: 10,
      textAlign: 'center'
    },
    directorText: {
      color: 'grey',
      fontSize: 12,
      marginBottom: 10,
      textAlign: 'center'
    },
    openingText: {
      textAlign: 'center'
    }
  });

export default SimpleAddItem