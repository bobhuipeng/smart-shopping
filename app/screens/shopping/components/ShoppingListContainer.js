import React, {
    Component
} from 'react'
import PropTypes from 'prop-types'
import {
    StyleSheet,
    Text,
    TextInput,
    Button,
    View,
    ListView,
    ScrollView,
    Alert,
} from 'react-native'


import {getTheme} from 'react-native-material-kit'
import Ionicons from 'react-native-vector-icons/Ionicons'


import ShoppingItem from '../containers/ShoppingItem'
import AddItem from  '../containers/AddItem'
import SimpleAddItem from './SimpleAddItem'
import ListShoppingItems from '../containers/ListShoppingItems'

import {shoppingListAPI} from '../../../api'

const theme = getTheme()

export default class ShoppingListContainer extends Component {

    static navigationOptions = {
        tabBarLabel: 'Shopping List',
        tabBarIcon: (
            { tintColor, focused }) => (
                <Ionicons
                  name={focused ? 'ios-home' : 'ios-home-outline'}
                  size={26}
                  style={{ color: tintColor }}
                />
        ),  
    }

    constructor(props) {
        super(props)
        this.ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        })
        let initList = this.props.itemList

        this.state = {
            shoppingListName : `Shopping List - ${(new Date()).toDateString()}`,//TODO: user remote API get from DB
            itemList: initList,
            dataSource: this.ds.cloneWithRows(initList),
            shoppingFinished: false,
            historyItems:[]
        }
    }

    async componentWillMount(){
        //TODO: add login username at here
       const historyItems = await shoppingListAPI.getShoppingItemHabit('stormswan')
       this.setState({
           historyItems:historyItems
       })
   }

     /**
     * this shoppingl list item has been done
     */
    finishItem = (itemId) => {
        this.updateItemDone(itemId,true)
    }

    submitShoppingList = () => {
      console.log('Submit shopping list to DB');
      let shoppingFinished = this.state.shoppingFinished;
      this.setState({
        shoppingFinished: !shoppingFinished
      })

      if(shoppingFinished){
          return ;
      }

      let spl = {
          name:this.state.shoppingListName,
          items: this.props.itemList
      }
      //TODO: udpate the username with login user      
      shoppingListAPI.submitShoppingList('stormswan',spl)
    }

    getReduxState = () => {
      console.log(store.getState())
    }
    

  
  render() {
    let addItemComponent = this.props.isSimpleMode ? <SimpleAddItem addNewItem={this.addNewItem} historyItems={this.state.historyItems}/> : 
                                                     <AddItem addNewItem={this.addNewItem} historyItems={this.state.historyItems}/>
    
    return (
        <View style={styles.listContainer}>
          <TextInput value= {this.state.shoppingListName}  style={styles.shoppingListTitle} 
                     onChangeText = {(txt) => this.setState({shoppingListName:txt})}/> 

          {!this.state.shoppingFinished && addItemComponent}

          <ScrollView>
          <ListShoppingItems/>
          </ScrollView>
          <View style={styles.centerLayout}>
              <View style={styles.finishedButton}>
                  <Text onPress={this.submitShoppingList}>
                      {this.state.shoppingFinished ?'Shopping Again' :'Finish Shopping' } 
                      <Ionicons
                          name='ios-cart-outline'
                          size={30}
                      />
                  </Text>
              </View>
          </View>
        </View>
    )
  }
}
const styles = StyleSheet.create({
    listContainer: {
        backgroundColor: '#F5FCFF',
        flex: 1,
        paddingTop: 25
    },
    centerLayout: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    finishedButton: {
        borderRadius: 15,
        alignItems: 'center',
        width: 150,
        height: 40,
        backgroundColor: 'steelblue'
    },
    shoppingListTitle: {
        flexDirection: 'row',
        margin: 10,
        padding: 5
    },
})
