import { connect } from 'react-redux'
import ListShoppingItems from '../components/ListShoppingItems'
import {addItem,updateItemQuantity} from '../../../redux/Actions'


const mapStateToProps = state => ({
  // todos: getVisibleTodos(state.todos, state.visibilityFilter)
  shopingItems: [...state.currentSPList], 
  shoppingFinished: false, //TODO: 
  simpleMode: state.sysSetting.isSimpleMode
})


export default connect(
  mapStateToProps
)(ListShoppingItems)
