import {connect} from 'react-redux'

import ShoppingItem from '../components/ShoppingItem'
import {increaseItemCount,decreaseItemCount,doneItem,deleteItem,redoItem, getUserHobitItemList} from '../../../redux/Actions'


const mapStateToProps = (state, props) =>({
  item: props.item,
  shoppingFinished:props.shoppingFinished
})


const mapDispatchToProps = dispatch =>({

  increaseItemCount(item){
    dispatch(increaseItemCount(item))
  },
  decreaseItemCount(item){
    dispatch(decreaseItemCount(item))
  },
  finishItem(item){
    dispatch(doneItem(item))
  },
  deleteItem(item){
    dispatch(deleteItem(item))
  },
  redoItem(item){
    dispatch(redoItem(item))
  }

})


export default connect(mapStateToProps,mapDispatchToProps)(ShoppingItem)