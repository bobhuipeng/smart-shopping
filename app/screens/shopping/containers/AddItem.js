import {connect} from 'react-redux'
// import Constants from '../../../redux/Constants'
import {addItem,updateItemQuantity} from '../../../redux/Actions'
import AddItem from '../components/AddItem'


const mapStateToProps = (state, props) =>({
  historyItems: state.habitItems === null || state.habitItems === undefined ?  [] :  state.habitItems,
  itemList:state.currentSPList
})


const mapDispatchToProps = dispatch =>({
  addNewItem(item){
    dispatch(addItem(item))
  },
  updateItemQuantity(item){
    dispatch(updateItemQuantity(item))
  }
})


export default connect(mapStateToProps,mapDispatchToProps)(AddItem)