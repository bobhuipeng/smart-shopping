import {connect} from 'react-redux'
// import Constants from '../../../redux/Constants'
import {addItem,updateItem} from '../../../redux/Actions'
import ShoppingListContainer from '../components/ShoppingListContainer'


const mapStateToProps = state =>({
   itemList:state.currentSPList,
   isSimpleMode:state.sysSetting.isSimpleMode
})


export default connect(mapStateToProps)(ShoppingListContainer)