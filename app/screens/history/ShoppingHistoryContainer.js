import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {  FlatList, ActivityIndicator, StyleSheet, Text, View, ScrollView } from 'react-native';


import Ionicons from 'react-native-vector-icons/Ionicons';
import {getTheme} from 'react-native-material-kit'


import listApi from '../../api/shoppingList'
import ItemView from './ItemView'

const theme = getTheme()

export default class ShoppingHistoryContainer extends Component {
//   static propTypes = {
//     prop: PropTypes
//   }


  static navigationOptions = {
    // title: 'React Course'
      tabBarLabel: 'Shopping List History',
      tabBarIcon: ({ tintColor, focused }) => (
        <Ionicons
          name={focused ? 'ios-settings' : 'ios-settings-outline'}
          size={26}
          style={{ color: tintColor }}
        />
      ),      
  }
  constructor(props) {
    super(props)
    this.state = {
      shoppingListHistory: [],
      isLoading: true
    }
  }

  componentDidMount() {
    this.props.getHistory()
  }

  refreshHistory = () => {
    this.props.getHistory()
  }
  

  render() {
      if(this.props.isLoading){
        return(
          <View style={{flex: 1, padding: 20}}>
            <ActivityIndicator/>
          </View>
        )
      }else{
        //TODO:  keyExtractor - remove index in the future.
        // It is only for this stage, because the shopping list name is not 
        // set to unique
        return(
  
          <ScrollView style={{flex: 1, paddingTop:20}}>
            <FlatList
              data={this.props.shoppingListHistory}
              renderItem={({item}) =><ItemView shoppingList = {item}/>}
              keyExtractor={(item) => item.name}
            />

              <View >
                  <Text onPress={this.refreshHistory}>
                    Refresh History
                      <Ionicons
                          name='ios-cart-outline'
                          size={30}
                      />
                  </Text>
              </View>
          </ScrollView>


        );
      }
  }
}
