
import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {  FlatList,  StyleSheet, Text, View } from 'react-native';

import { ListItem } from 'react-native-elements'

export default class ItemView extends PureComponent {
  static propTypes = {
    shoppingList: PropTypes.object.isRequired
  }

  constructor(props){
    super(props)
    this.state ={showListDetails:false}
  }

  render() {
    let shoppingFinished = true;
    let today = new Date();
    let allItem = this.props.shoppingList.items//.sort((itema,itemb) => itema.name > itemb.name ? 1 : 0)//.sort((itema,itemb) => itema.isDone? 1: 0)
    let shoppingItems =[]
    let buyedItem = allItem.filter((item)=> item.isDone).sort((a,b) => a.name > b.name )
    let unBuyedItem = allItem.filter((item)=>!item.isDone).sort((a,b) => a.name > b.name )
    Array.prototype.push.apply(shoppingItems, buyedItem);
    Array.prototype.push.apply(shoppingItems, unBuyedItem);
    let subTile =`Shopping at ${today.toLocaleDateString()}: buy ${buyedItem.length} of ${shoppingItems.length}`
    
    return (
        <View>
          <ListItem
          key={this.props.shoppingList.nam}
          title={this.props.shoppingList.name}
          subtitle={
            <View style={styles.subtitleView}>
              <Text style={styles.ratingText}>{subTile}</Text>
            </View>
          }
          leftIcon={{ name: 'assignment' }}
          rightIcon ={{ name: 'list' }}
          badge={{ value: shoppingItems.length, textStyle: { color: 'orange' }, containerStyle: { marginTop: -20 } }}
          onPress ={(e)=>{this.setState({showListDetails:!this.state.showListDetails})}}
        />
        <View style={[styles.shoppingItemContainer,this.state.showListDetails ? styles.shoppingList : styles.hideShoppingList]}>
          <FlatList
              data={shoppingItems}
              renderItem={({item}) =>
                          <View style={{flex: 1, flexDirection: 'row',}}>
                            <View style={[styles.shoppingItemDetails,{backgroundColor: item.isDone? 'lawngreen' : (shoppingFinished ? 'lightgray'  : 'powderblue')}]}>
                              <Text>{item.name} : {item.quantity} - {item.unit}</Text>
                            </View>
                          </View>
              }
              keyExtractor={(item, index) => item.name}
            />
        </View>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  shoppingItemContainer:{
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    // alignItems: 'center',
  },
  shoppingItemDetails: {
    flex: 0.7,
    height: 30,
    // width:350,
    // paddingLeft: 50,
    marginLeft: 50,
  },
  hideShoppingList: {
    width: 0,
    height: 0
  },
  shoppingList: {
    opacity: 100,
  },
  subtitleView: {
    flexDirection: 'row',
    paddingLeft: 10,
    paddingTop: 5
  },
  ratingImage: {
    height: 19.21,
    width: 100
  },
  ratingText: {
    paddingLeft: 10,
    color: 'grey'
  }
})