import {connect} from 'react-redux'
import {getShoppingListHistory} from '../../../redux/Actions'
import ShoppingHistoryContainer from '../ShoppingHistoryContainer'


const mapStateToProps = (state, props) =>({
  isLoading:state.historicalSPList.isFetching,
  shoppingListHistory: state.historicalSPList.historyList
})


const mapDispatchToProps = dispatch =>({
  getHistory(username){
    dispatch(getShoppingListHistory(username))
  }
})


export default connect(mapStateToProps,mapDispatchToProps)(ShoppingHistoryContainer)