import React, { Component } from 'react';
import { View,Text } from 'react-native'
import ShoppingListContainer from './shopping/containers/ShoppingListContainer'
export default class ShoppingListScreen extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return <ShoppingListContainer/>
  }
}