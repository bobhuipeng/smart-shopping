import {
  connect
} from 'react-redux'

import SysSetting from '../SysSetting'

import {
  tongleModelIsSimple,
  resetSysDefault
} from '../../../redux/Actions';

mapStateToProps = (state, props) => ({
  isSimpleMode: state.sysSetting.isSimpleMode
})


mapDispatchToProps = dispatch => ({
  tongleModelIsSimple(){
    dispatch(tongleModelIsSimple())
  },
  resetSysToDefault(){
    dispatch(resetSysDefault())
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(SysSetting)