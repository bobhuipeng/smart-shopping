import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableHighlight,Button } from 'react-native'

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
export class SysSetting extends Component {

    
    onPressButton = ()=>{
        this.props.tongleModelIsSimple()
    }  

    resetDefault = () =>{
      this.props.resetSysToDefault()
    }

  render() {
      
    let simpleSelectImage = this.props.isSimpleMode ? 'check-box' : 'check-box-outline-blank'
    return (
        <View style={styles.settingContainer}>
        
          <View style={[styles.settingButton]}>
          <Text>Use Simple Shopping List Model</Text>
            <TouchableHighlight onPress={this.onPressButton} underlayColor='yellow'>
                <MaterialIcons name={simpleSelectImage} size={32}/>
            </TouchableHighlight>
          </View>
          <View style={[styles.settingButton]}>
            <Text onPress={this.resetDefault}>Reset System Default Value  
              <Ionicons name='ios-settings' size = {32} />
            </Text>
          </View>
        </View>

    )
  }
}


const styles = StyleSheet.create({
    settingContainer:{
        paddingTop:50
    },
    settingButton: {
        // flex: 0.25,
        justifyContent: 'space-around',
        flexDirection: 'row',
        backgroundColor: 'white',
        // borderRadius: 12,
        // borderWidth: 2.5,
        // borderColor: 'darkorange',
        margin:0,
        padding:0,
    },
})

export default SysSetting