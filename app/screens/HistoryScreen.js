import React, { Component } from 'react';
import { View,Text } from 'react-native'
import ShoppingHistoryContainer from './history/containers/ShoppingHistoryContainer'
export default class HistoryScreen extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return <ShoppingHistoryContainer/>
  }
}