import React from 'react';
import { Provider } from 'react-redux'

import storeFactory from './app/redux'

import AppNavigator from './app/AppNavigator'


const store = storeFactory()

// window.React = React
// window.store = store

 export default class App extends React.Component {
     render() {
       return (
        <Provider store = {store}>
         <AppNavigator/>
         </Provider>
       );
     }
   }
 

